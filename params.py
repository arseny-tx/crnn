import alphabets

# about data and net
alphabet = alphabets.alphabet

import string
alphabet = string.printable[:62]

keep_ratio = False # whether to keep ratio for image resize
manualSeed = 1234 # reproduce experiemnt
# random_sample = True # whether to sample the dataset with random sampler
random_seq_sample = True # whether to sample the dataset with random sampler
shuffle = False

imgH = 32 # the height of the input image to network
imgW = 100 # the width of the input image to network
nh = 256 # size of the lstm hidden state

# nc = 3
nc = 1

pretrained = '' # path to pretrained model (to continue training)
# expr_dir = 'expr' # where to store samples and models
dealwith_lossnan = True # whether to replace all nan/inf in gradients to zero

# hardware
cuda = True # enables cuda
multi_gpu = False # whether to use multi gpu
ngpu = 1 # number of GPUs to use. Do remember to set multi_gpu to True!
workers = 10 # number of data loading workers

# training process
displayInterval = 100 # interval to be print the train loss
valInterval = 5 # interval to val the model loss and accuray
saveInterval = 20 # interval to save model
n_val_disp = 10 # number of samples to display when val the model

# finetune
nepoch = 1000 # number of epochs to train for
batchSize = 64 # input batch size
lr = 0.0001 # learning rate for Critic, not used by adadealta
beta1 = 0.5 # beta1 for adam. default=0.5
adam = False # whether to use adam (default is rmsprop)
adadelta = False # whether to use adadelta (default is rmsprop)


"""

Traceback (most recent call last):                                                                                                                    [19/1214]
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/queues.py", line 236, in _feed
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 51, in dumps
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/site-packages/torch/multiprocessing/reductions.py", line 337, in reduce_storage
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 194, in DupFd
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/resource_sharer.py", line 48, in __init__
OSError: [Errno 24] Too many open files
Traceback (most recent call last):
Process Process-7:
Traceback (most recent call last):
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/resource_sharer.py", line 149, in _serve
    send(conn, destination_pid)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/resource_sharer.py", line 50, in send
    reduction.send_handle(conn, new_fd, pid)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 179, in send_handle
    with socket.fromfd(conn.fileno(), socket.AF_UNIX, socket.SOCK_STREAM) as s:
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/socket.py", line 463, in fromfd
    nfd = dup(fd)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/process.py", line 297, in _bootstrap
    self.run()
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/process.py", line 99, in run
    self._target(*self._args, **self._kwargs)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/site-packages/torch/utils/data/_utils/worker.py", line 160, in _worker_loop
    r = index_queue.get(timeout=MP_STATUS_CHECK_INTERVAL)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/queues.py", line 113, in get
    return _ForkingPickler.loads(res)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/site-packages/torch/multiprocessing/reductions.py", line 294, in rebuild_storage_fd
    fd = df.detach()
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/resource_sharer.py", line 58, in detach
    return reduction.recv_handle(conn)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 185, in recv_handle
    return recvfds(s, 1)[0]
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 155, in recvfds
    raise EOFError
EOFError
OSError: [Errno 24] Too many open files
TRAIN: Exception DataLoader worker (pid(s) 25051) exited unexpectedly







Traceback (most recent call last):
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/queues.py", line 236, in _feed
    obj = _ForkingPickler.dumps(obj)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 51, in dumps
    cls(buf, protocol).dump(obj)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/site-packages/torch/multiprocessing/reductions.py", line 337, in reduce_storage
    df = multiprocessing.reduction.DupFd(fd)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/reduction.py", line 194, in DupFd
    return resource_sharer.DupFd(fd)
  File "/media/disk1/anerinovsky/anaconda3/envs/c3d/lib/python3.7/multiprocessing/resource_sharer.py", line 48, in __init__
    new_fd = os.dup(fd)
OSError: [Errno 24] Too many open files


"""