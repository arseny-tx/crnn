from __future__ import print_function
from __future__ import division

import argparse
import random
import torch
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import numpy as np
# from warpctc_pytorch import CTCLoss
from torch.nn import CTCLoss
import os
import utils
import dataset

import lmdb

import models.crnn as net
import params


import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

parser = argparse.ArgumentParser()
parser.add_argument('-train', '--trainroot', required=True, help='path to train dataset')
parser.add_argument('-val', '--valroot', required=True, help='path to val dataset')
parser.add_argument('--batch_size', required=True, type=int)
parser.add_argument('--num_workers', required=True, type=int)
parser.add_argument('--expr_dir', required=True, type=str)

args = parser.parse_args()

if not os.path.exists(args.expr_dir):
    os.makedirs(args.expr_dir)


params.batchSize = args.batch_size
params.workers = args.num_workers

# ensure everytime the random is the same
random.seed(params.manualSeed)
np.random.seed(params.manualSeed)
torch.manual_seed(params.manualSeed)

# cudnn.benchmark = True

if torch.cuda.is_available() and not params.cuda:
    print("WARNING: You have a CUDA device, so you should probably set cuda in params.py to True")

# -----------------------------------------------
"""
In this block
    Get train and val data_loader
"""
from pathlib import Path

def data_loader(args):

    # train
    train_dataset = dataset.lmdbDataset(root=args.trainroot)
    # train_dataset = dataset.JmsynthDataset(root=args.trainroot, train=True)
    assert train_dataset
    if params.random_seq_sample:
        sampler = dataset.randomSequentialSampler(train_dataset, params.batchSize)
    else:
        sampler = None

    mk_ds = lambda ds, sampler: torch.utils.data.DataLoader(ds, batch_size=params.batchSize, \
            shuffle=params.shuffle, sampler=sampler, num_workers=int(params.workers), \
            collate_fn=dataset.alignCollate(imgH=params.imgH, imgW=params.imgW, keep_ratio=params.keep_ratio))

    print('Workers', int(params.workers))
    train_loader = mk_ds(train_dataset, sampler)
    
    # val
    # val_dataset = dataset.JmsynthDataset(root=args.valroot, train=False)
    # val_dataset = dataset.lmdbDataset(root=args.valroot, transform=dataset.resizeNormalize((params.imgW, params.imgH)))
    val_dataset = dataset.lmdbDataset(root=args.valroot)
    assert val_dataset


    # val_loader = torch.utils.data.DataLoader(val_dataset, 
    #         shuffle=params.shuffle, sampler=sampler, batch_size=params.batchSize, num_workers=int(params.workers), 
    #         collate_fn=dataset.alignCollate(imgH=params.imgH, imgW=params.imgW, keep_ratio=params.keep_ratio)
    #         )
    
    val_loader = mk_ds(val_dataset, None)
    

    return train_loader, val_loader

train_loader, val_loader = data_loader(args)

# -----------------------------------------------
"""
In this block
    Net init
    Weight init
    Load pretrained model
"""
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

def net_init():
    nclass = len(params.alphabet) + 1
    crnn = net.CRNN(params.imgH, params.nc, nclass, params.nh)
    crnn.apply(weights_init)
    if params.pretrained != '':
        print('loading pretrained model from %s' % params.pretrained)
        if params.multi_gpu:
            crnn = torch.nn.DataParallel(crnn)
        crnn.load_state_dict(torch.load(params.pretrained))
    
    return crnn

crnn = net_init()
print(crnn)

# -----------------------------------------------
"""
In this block
    Init some utils defined in utils.py
"""
# Compute average for `torch.Variable` and `torch.Tensor`.
loss_avg = utils.averager()

# Convert between str and label.
converter = utils.strLabelConverter(params.alphabet)

# -----------------------------------------------
"""
In this block
    criterion define
"""
criterion = CTCLoss()

# -----------------------------------------------
"""
In this block
    Init some tensor
    Put tensor and net on cuda
    NOTE:
        image, text, length is used by both val and train
        becaues train and val will never use it at the same time.
"""
image = torch.FloatTensor(params.batchSize, 3, params.imgH, params.imgH)
text = torch.LongTensor(params.batchSize * 5)
length = torch.LongTensor(params.batchSize)

if params.cuda and torch.cuda.is_available():
    criterion = criterion.cuda()
    image = image.cuda()
    text = text.cuda()

    crnn = crnn.cuda()
    if params.multi_gpu:
        crnn = torch.nn.DataParallel(crnn, device_ids=range(params.ngpu))

image = Variable(image)
text = Variable(text)
length = Variable(length)

# -----------------------------------------------
"""
In this block
    Setup optimizer
"""
if params.adam:
    optimizer = optim.Adam(crnn.parameters(), lr=params.lr, betas=(params.beta1, 0.999))
elif params.adadelta:
    optimizer = optim.Adadelta(crnn.parameters())
else:
    optimizer = optim.RMSprop(crnn.parameters(), lr=params.lr)

# -----------------------------------------------
"""
In this block
    Dealwith lossnan
    NOTE:
        I use different way to dealwith loss nan according to the torch version. 
"""
if params.dealwith_lossnan:
    if torch.__version__ >= '1.1.0':
        """
        zero_infinity (bool, optional):
            Whether to zero infinite losses and the associated gradients.
            Default: ``False``
            Infinite losses mainly occur when the inputs are too short
            to be aligned to the targets.
        Pytorch add this param after v1.1.0 
        """
        criterion = CTCLoss(zero_infinity = True)
    else:
        """
        only when
            torch.__version__ < '1.1.0'
        we use this way to change the inf to zero
        """
        crnn.register_backward_hook(crnn.backward_hook)

# -----------------------------------------------

from tqdm import tqdm
from jiwer import wer

def val(net, criterion, max_iter=None):
    print('Start val')

    for p in crnn.parameters():
        p.requires_grad = False

    net.eval()
    val_iter = iter(val_loader)
    preds = None
    wers = 0
    i = 0
    n_correct = 0
    loss_avg = utils.averager() # The blobal loss_avg is used by train

    max_iter = max_iter or len(val_loader) 

    for i in tqdm(range(max_iter)):
        try:
            data = val_iter.next()
        except Exception as e:
            print('VAL: Exception', e)
            continue
        i += 1
        cpu_images, cpu_texts = data
        batch_size = cpu_images.size(0)
        utils.loadData(image, cpu_images)
        t, l = converter.encode(cpu_texts)
        utils.loadData(text, t)
        utils.loadData(length, l)

        preds = crnn(image)
        preds_size = Variable(torch.LongTensor([preds.size(0)] * batch_size))
        cost = criterion(preds, text, preds_size, length) / batch_size
        loss_avg.add(cost)

        _, preds = preds.max(2)
        preds = preds.transpose(1, 0).contiguous().view(-1)
        sim_preds = converter.decode(preds.data, preds_size.data, raw=False)
        cpu_texts_decode = []
        for i in cpu_texts:
            cpu_texts_decode.append(i.decode('utf-8', 'strict'))            

        for pred, target in zip(sim_preds, cpu_texts_decode):
            # target = target.decode('utf-8')
            if pred == target:
                n_correct += 1
            
            wers += wer(target, pred)

    raw_preds = converter.decode(preds.data, preds_size.data, raw=True)[:params.n_val_disp]
    for raw_pred, pred, gt in zip(raw_preds, sim_preds, cpu_texts_decode):
        print('%-20s => %-20s, gt: %-20s' % (raw_pred, pred, gt))

    accuracy = n_correct / float(max_iter * params.batchSize)
    wers = wers / float(max_iter * params.batchSize)

    print('Val loss: %f, accuray: %f, wer %f' % (loss_avg.val(), accuracy, wers))



from timeit import default_timer as timer

def train(net, criterion, optimizer, train_iter):

    timers = []

    for p in crnn.parameters():
        p.requires_grad = True
    crnn.train()


    b = timer()
    try:
        
        data = train_iter.next()
    except Exception as e:
        print('TRAIN: Exception', e)
        return None, None
    e = timer()
    timers.append(e-b)

    b = timer()
    cpu_images, cpu_texts = data
    batch_size = cpu_images.size(0)
    utils.loadData(image, cpu_images)
    t, l = converter.encode(cpu_texts)
    utils.loadData(text, t)
    utils.loadData(length, l)
    e = timer()
    timers.append(e-b)

    b = timer()
    optimizer.zero_grad()
    preds = crnn(image)
    preds_size = Variable(torch.LongTensor([preds.size(0)] * batch_size))
    cost = criterion(preds, text, preds_size, length) / batch_size
    # crnn.zero_grad()
    cost.backward()
    optimizer.step()
    e = timer()
    timers.append(e-b)

    return cost, timers


if __name__ == "__main__":
    b = timer()
    for epoch in range(params.nepoch):
        train_iter = iter(train_loader)
        i = 0
        timers = []
        while i < len(train_loader):

            # val(crnn, criterion, 20)

            cost, _timers = train(crnn, criterion, optimizer, train_iter)
            if cost is None:
                continue
            loss_avg.add(cost)
            timers.append(_timers)
            i += 1
            print('.', end='')
            if i % params.displayInterval == 0:
                print()
                print('[%d/%d][%d/%d] Loss: %f' %
                      (epoch, params.nepoch, i, len(train_loader), loss_avg.val()))
                loss_avg.reset()
                timers = np.array(timers)

                print('    ', 'data-form-iter, data-to-gpu, net-forward, total')
                print('mean', timers.mean(0), timers.sum(1).mean())
                print('std ', timers.std(0), timers.sum(1).std())

                print('Time spent', timer() - b)

                timers = []

        if epoch % params.saveInterval == 0 or i == 0:
            torch.save(crnn.state_dict(), '{0}/netCRNN_{1}_{2}.pth'.format(args.expr_dir, epoch, i))

        if epoch % params.valInterval == 0:
            val(crnn, criterion)

    torch.save(crnn.state_dict(), '{0}/netCRNN_last.pth'.format(args.expr_dir))